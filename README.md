# R Workshop Lucerne

## Setup

The working basis is a running _R (>=3.5)_ installation. It is also recommended to use the free IDE _RStudio (>= 1.1)_ during the course (https://www.rstudio.com/products/rstudio/download/). Please also install the following packages. All are available on CRAN:

- tidyverse
- ggthemes
- ggrepel
- gghighlight
- Hmisc
- openxlsx
- shiny
- shinythemes
- rmarkdown

## Data and resources

Please use the ESS CSV data sets in the data folder.

## Subject Day 1 - Tidyverse

The course is divided into two parts. **Day 1** focuses on the _Tidyverse_. It consists of a number of coordinated packages that implement modern informatics concepts in R. The structure of the course is based on the commonly known _Data Science Workflow_ (https://r4ds.had.co.nz/introduction.html). It often concentrates on certain operations and the corresonating packages. Following a general introduction to the tidyverse and cutting edge data science tools (e.g. git versioning or package project organisation), you will learn:

- _Tibbles_, the basic data structure of the Tidyverse: What are they? How do they differ from "old" data frames? Convert a data frame to a tibble and back.
- Import and store data with _readr_, _readxl_ and _openxlsx_: Read and write data. Benefit from the consistency and strictness of the tidyverse. Practical hints for data organization.
- Tidy data with _tidyr_: Get data sets into "tidy" form. Gather multiple columns in key-value pairs or spread key-value pairs into multiple columns.
- The **spotlight** of the tidyverse and day one: Transform data with _dplyr_. Filter, summarise and group data with ease. Get in touch with the pipe operator (%>%) and the fancy syntax and intuitive grammar of the tidyverse.
- Visualise data using the grammar of graphcis, _ggplot2_: Ingerate ggplot2 into the tidyverse workflow. Creation of high-quality publication-ready plots. Saving plot-objects for further work.

## Preparation Day 1

In preparation, it is a good idea to browse the _tidyverse website_: https://www.tidyverse.org/. There you will find a short information page (focussing the core functions) for each package mentioned earlier. Another good source of information is the book _R for Data Science_ by Garrett Grolemund & Hadley Wickham. It is available online: https://r4ds.had.co.nz/. The _RStudio website_ also has cheatsheets available: https://www.rstudio.com/resources/cheatsheets/. A look at the "Data Import" and "Data Transformation" cheatsheet may be helpful.

## Subject Day 2 - Shiny

The **second day** deals with interactive web apps. With _Shiny_ it is possible to create R-based web applications without much effort. These can for example enrich teaching or be used to develop interactive presentations of research findings. This makes it possible to convey complex statistical interrelationships without the recipients having to have a proven R-knowledge. In fact, they don't need to know anything at all about R.

- Shiny app architecture: Learn how to structure Shiny applications. Server and user interface code. App formats and launching apps
- Reactivity: Understanding the reactive programming model used by Shiny.
- Build fancy user interfaces: Control the general appearance. Input and output elements. Showcase mode as an interesting tool for teaching.
- Deployment: Release of the Shiny app. Providers and possibilities of own distribution.

## Preparation Day 2

The _interactive learning session from RStudio_ to Shiny is a good preparation tool: http://shiny.rstudio.com/tutorial/written-tutorial/lesson1/. However, it is not necessary to work through it in detail. It's more about getting a first impression of Shiny. A _cheatsheet_ also exists: http://shiny.rstudio.com/articles/cheatsheet.html.
